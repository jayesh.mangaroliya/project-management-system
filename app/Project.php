<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'project_name', 'project_start_date','project_value','project_currency','project_value_EUR'
    ];
}
