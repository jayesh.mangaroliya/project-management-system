{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
<html>
<head>
    <title>Project Management System</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.css')
    @include('layouts.js')
</head>
<body class="bg-dark">
 <div class="container">
   <div class="row">
    @if(auth()->user()->role == "admin")
     <div class="col-md-12">
       <div class="card mt-5">
         <div class="card-header">
            <div class="col-md-12">
                <h4 class="card-title">Project Management System
                  <a class="btn btn-success ml-5" href="javascript:void(0)" id="createNewItem" style="margin-left: 30rem!important;"> Create New Item</a>
                  <a class="btn btn-success" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                </h4>
            </div>
         </div>
         <div class="card-body">
           <table class="table table-bordered data-table">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>Name</th>
                        <th>Start Date</th>
                        <th>Value</th>
                        <th>Currency Rate</th>
                        <th>ValueEUR</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        @else
        <div class="card-header">
            <div class="col-md-12">
                <h4 style="color: white">You are normal user not access admin panel
                    <a class="btn btn-success" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                     {{ __('Logout') }}
                 </a>

                 <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                     @csrf
                 </form>
                </h4>
            </div>
         </div>
        @endif
        <div class="modal fade" id="ajaxModel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>
                    <div class="modal-body">
                        <form id="ItemForm" name="ItemForm" class="form-horizontal">
                           <input type="hidden" name="Item_id" id="Item_id">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="project_name" name="project_name" placeholder="Enter Name" value="" maxlength="50" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Date</label>
                                <div class="col-sm-12">
                                    <input type="date" class="form-control" id="project_start_date" name="project_start_date" placeholder="date" value="" maxlength="50" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">Value</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="project_value" name="project_value" placeholder="Enter Value" value="" maxlength="50" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">Currency</label>
                                <div class="col-sm-12">
                                    <select name="project_currency" id="project_currency" class="form-control">
                                        <option value="0">Please Select Currency</option>
                                        <option value="2">USD</option>
                                        <option value="2.5">JPY</option>
                                        <option value="3">GBP</option>
                                      </select>
                                  </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">ValueEUR</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="project_value_EUR" name="project_value_EUR" placeholder="Enter Value" value="" maxlength="50" required="">
                                </div>
                            </div>
                            <div class="col-sm-offset-2 col-sm-10">
                             <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                             </button>
                             <button type="reset" class="btn btn-primary" id="reset" value="reset">Reset
                            </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</body>
<script type="text/javascript">
$(document).on('change keyup blur', "#project_value, #project_currency", function() {
    var val1 = $("#project_value").val()
    var val2 = $("#project_currency").val()
    var result = val1 * val2
    $("#project_value_EUR").val(result)
});

  $(function () {

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('ajaxItems.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'project_name', name: 'project_name'},
            {data: 'project_start_date', name: 'project_start_date'},
            {data: 'project_value', name: 'project_value'},
            {data: 'project_currency', name: 'project_currency'},
            {data: 'project_value_EUR', name: 'project_value_EUR'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    $('#createNewItem').click(function () {
        $('#saveBtn').val("create-Item");
        $('#Item_id').val('');
        $('#ItemForm').trigger("reset");
        $('#modelHeading').html("Create New Item");
        $('#ajaxModel').modal('show');
    });

    $('body').on('click', '.editItem', function () {
      var Item_id = $(this).data('id');
      $.get("{{ route('ajaxItems.index') }}" +'/' + Item_id +'/edit', function (data) {
          $('#modelHeading').html("Edit Item");
          $('#saveBtn').val("edit-user");
          $('#ajaxModel').modal('show');
          $('#Item_id').val(data.id);
          $('#project_name').val(data.project_name);
          $('#project_start_date').val(data.project_start_date);
          $('#project_value').val(data.project_value);
          $('#project_currency').val(data.project_currency);
          $('#project_value_EUR').val(data.project_value_EUR);
      })
   });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        // $(this).html('Sending..');

        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "{{ route('ajaxItems.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {

              $('#ItemForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              table.draw();

          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
    });

    $('body').on('click', '.deleteItem', function () {

        var Item_id = $(this).data("id");

        swal({
            title: "Are you sure!",
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes!",
            showCancelButton: true,
        },
        function() {
            $.ajax({
            type: "DELETE",
            url: "{{ route('ajaxItems.store') }}"+'/'+Item_id,
            success: function (data) {
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });

    });

  });

</script>
</html>
